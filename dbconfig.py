'''
im_post:
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcanalesocial` int(11) DEFAULT NULL,
  `content` text,
  `link` text,
  `likes` int(11) DEFAULT NULL,
  `var_likes` int(11) DEFAULT '0',
  `comments` int(11) DEFAULT NULL,
  `var_comments` int(11) DEFAULT '0',
  `shares` int(11) DEFAULT NULL,
  `var_shares` int(11) DEFAULT '0',
  `img` text,
  `originalIdPost` text,
  `timestamp` int(11) DEFAULT NULL,
  `inserted_on` int(11) DEFAULT NULL,
  `updated_on` int(11) DEFAULT NULL,
  `postlink` text,
  PRIMARY KEY (`id`),


post_lang:
  `id` int(11) NOT NULL,
  `lang` varchar(16),
  PRIMARY KEY (`id`),

English_content:
  `id` int(11) NOT NULL,
  `lang` TEXT,
  PRIMARY KEY (`id`),
'''

config = {
  'user': '',
  'password': '',
  'host': 'localhost',
  'database': 'im_post',
  'raise_on_warnings': True
}
