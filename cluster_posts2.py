#!/usr/bin/env python3

import dbconfig

import itertools
import re
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import mysql.connector


import nltk
import gensim

"""
 - Find the optimal number of clusters using the elbow method
 - Find and represent the clusters using k-means
 - Find the topics and associated posts using LDA
"""

def load_stopwords():
    stop_words = nltk.corpus.stopwords.words('english')
    stop_words.extend(['this', 'that', 'the', 'might', 'have', 'been', 'from',
                       'but', 'they', 'will', 'has', 'having', 'had', 'how',
                       'went' 'were', 'why', 'and', 'still', 'his',
                       'her', 'was', 'its', 'per', 'cent',
                       'a', 'able', 'about', 'across', 'after', 'all', 'almost', 'also', 'am', 'among',
                       'an', 'and', 'any', 'are', 'as', 'at', 'be', 'because', 'been', 'but', 'by', 'can',
                       'cannot', 'could', 'dear', 'did', 'do', 'does', 'either', 'else', 'ever', 'every',
                       'for', 'from', 'get', 'got', 'had', 'has', 'have', 'he', 'her', 'hers', 'him', 'his',
                       'how', 'however', 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'just', 'least', 'let',
                       'like', 'likely', 'may', 'me', 'might', 'most', 'must', 'my', 'neither', 'nor',
                       'not', 'of', 'off', 'often', 'on', 'only', 'or', 'other', 'our', 'own', 'rather', 'said',
                       'say', 'says', 'she', 'should', 'since', 'so', 'some', 'than', 'that', 'the', 'their',
                       'them', 'then', 'there', 'these', 'they', 'this', 'tis', 'to', 'too', 'twas', 'us',
                       'wants', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while', 'who',
                       'whom', 'why', 'will', 'with', 'would', 'yet', 'you', 'your', 've', 're', 'rt', 'retweet',
                       '#fuckem', '#fuck',
                       'fuck', 'ya', 'yall', 'yay', 'youre', 'youve', 'ass', 'factbox', 'com', '&lt', 'th',
                       'retweeting', 'dick', 'fuckin', 'shit', 'via', 'fucking', 'shocker', 'wtf', 'hey', 'ooh',
                       'rt&amp', '&amp',
                       '#retweet', 'retweet', 'goooooooooo', 'hellooo', 'gooo', 'fucks', 'fucka', 'bitch', 'wey',
                       'sooo', 'helloooooo', 'lol', 'smfh'])
    stop_words = set(stop_words)
    return stop_words


def fetch_data():
    """
    Connects to DB, fethes all data, returns a list of posts.

    Returns:
        list: list of strings (tweets)
    """
    cnx = mysql.connector.connect(**dbconfig.config)
    cursor = cnx.cursor()

    QUERY_SELECT = ("SELECT content FROM im_post WHERE id in "
                    "(SELECT id from post_lang_langid WHERE lang='en' AND probability >= 0.9) "
                    "AND content LIKE '%#%' "
                    "ORDER BY RAND() LIMIT 5000")
    cursor.execute(QUERY_SELECT)
    post_list = []
    for row in cursor:
        post_list.append(row[0])

    cnx.close()
    return post_list


def iter_post(post_list, stoplist):
    """Yields tokenized items of a single posts in a list
    Returns tokenized and stemmed lists of items post by post instead of
    returning a list of all tokenized words.

    Args:
        post_list (list): list of posts(string)
        stoplist (set): set of stop words
    Yields:
        list: The tokenized list from posts
    """

    #there is no stemmer in gensim
    for post in post_list:
        # in some cases
        yield re.findall("\B#\w*[a-z]+\w*", post.lower())


class MyCorpus(object):
    """
    Yield each document in turn, as a list of tokens (unicode strings).
    Converts a collection of words to its bag-of-words representation:
    a list of (word_id, word_frequency)
    """
    def __init__(self, post_list, stoplist):
        self.post_list = post_list
        self.stoplist = stoplist
        self.dictionary = gensim.corpora.Dictionary(iter_post(post_list, stoplist))

    def __iter__(self):
        """
        Yields a couple (id, frequency)
        """
        for tokens in iter_post(self.post_list, self.stoplist):
            yield self.dictionary.doc2bow(tokens)


def main():

    post_list = []
    post_list = fetch_data()

    # takes a DB stream, prints first 3 test articles
    print(post_list[:3])

    # prints first 3 tokenized lists of posts
    for tokens in itertools.islice(iter_post(post_list,
    load_stopwords()), 3):
        print("Printing tokens")
        # print its first ten tokens
        print(tokens[:10])

    """
    post_gen is an example how to use properly a generator, it doesn't
    have any other useful purpose.
    """
    # (tokens for tokens in iter_post(post_list)) - constructs a generator
    post_gen = (tokens for tokens in iter_post(post_list, load_stopwords()))
    """generator: list of strings
    Construct a generator which yields post by post the list of tokens.
    """

    # prints n posts from generator
    print("\n\n#prints n posts from generator\n")
    for x in range(2):
        print(next(post_gen))
        print()

    """
    The next step is to convert the texts to a format that gensim can use - namely
    a Bag of Words (BoW) representation. Gensim expects to be fed a corpus data
    structure, basically a list of sparse vectors. The sparse vector elements
    consists of (id, score) pairs, where the id is a numeric ID that is mapped to
    the term via a dictionary.
    """

    #Create BOW object
    corpus = MyCorpus(post_list, load_stopwords())

    """dict: {id: word}
    gensim.Dictionary is an object that map into raw text tokens (strings) from
    their numerical ids (integers).id2word holds only unique words from all
    available posts.
    """

    # ignore words that appear in less than 50 documents or more than 5% documents
    corpus.dictionary.filter_extremes(no_below=0.05, no_above=100)

    #Save {id: word_id} object
    corpus.dictionary.save("mtsamples.dict")
    #convert tokenized documents to vectors
    gensim.corpora.MmCorpus.serialize("mtsamples.mm", corpus)

    dictionary = gensim.corpora.Dictionary.load("mtsamples.dict")
    corpus = gensim.corpora.MmCorpus("mtsamples.mm")

    """"simple transformation which takes documents represented as
    bag-of-words counts and applies a weighting which discounts common
    terms (or, equivalently, promotes rare terms). It also scales the
    resulting vector to unit length """
    tfidf = gensim.models.TfidfModel(corpus, normalize=True)
    #list (id_word, tfid_value)
    corpus_tfidf = tfidf[corpus]

    # project to 2 dimensions for visualization
    lsi = gensim.models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2)

    # write out coordinates to file
    fcoords = open( "coords.csv", 'wt')
    for vector in lsi[corpus]:
        if len(vector) != 2:
            continue
        #print("%6.4f\t%6.4f\n" % (vector[0][1], vector[1][1]))
        fcoords.write("%6.4f\t%6.4f\n" % (vector[0][1], vector[1][1]))
    fcoords.close()

    #Start elbow analysis: choose # of k
    MAX_K = 10

    X = np.loadtxt("coords.csv", delimiter="\t")
    ks = range(1, MAX_K + 1)

    inertias = np.zeros(MAX_K)
    diff = np.zeros(MAX_K)
    diff2 = np.zeros(MAX_K)
    diff3 = np.zeros(MAX_K)
    for k in ks:
        kmeans = KMeans(k).fit(X)
        # inertia or within-cluster sum-of-squares
        inertias[k - 1] = kmeans.inertia_
        # first difference
        if k > 1:
            diff[k - 1] = inertias[k - 1] - inertias[k - 2]
        # second difference
        if k > 2:
            diff2[k - 1] = diff[k - 1] - diff[k - 2]
        # third difference
        if k > 3:
            diff3[k - 1] = diff2[k - 1] - diff2[k - 2]

    elbow = np.argmin(diff3[3:]) + 1

    # Plot elbow
    plt.plot(ks, inertias, "b*-")
    plt.plot(ks[elbow], inertias[elbow], marker='o', markersize=12,
             markeredgewidth=2, markeredgecolor='r', markerfacecolor=None)
    plt.ylabel("Inertia")
    plt.xlabel("K")
    plt.figure(1)
    plt.draw()

    NUM_TOPICS = ks[elbow]

    # Find clusters with k-means and plot them
    kmeans = KMeans(NUM_TOPICS).fit(X)
    y = kmeans.labels_
    plt.figure(2)
    colors = ["blue", "g", "r", "m", "c", "yellow", "pink", "black", "purple"]
    scatter_x = []
    scatter_y = []
    scatter_colors = []
    for i in range(X.shape[0]):
        scatter_x.append(X[i][0])
        scatter_y.append(X[i][1])
        scatter_colors.append(colors[y[i]])
    plt.scatter(scatter_x, scatter_y, c=scatter_colors, s=50)
    plt.draw()


    # Project to LDA space
    lda = gensim.models.LdaModel(corpus, id2word=dictionary, num_topics=NUM_TOPICS)
    lda.print_topics(NUM_TOPICS)

    print("\n\nTop words: ")
    top_words = [[word for (word, _) in lda.show_topic(topicno, topn=5)]
                for topicno in range(lda.num_topics)]
    #print topic terms
    for x in range(len(top_words)):
        print("Topic %d:" %(x+1))
        print(top_words[x])

    print("\nTopic probability distribution of post #0:")
    for (topic, probability) in lda[corpus][0]:
        print(" Topic %d: %f" % (topic, probability))

    # Keep plots open
    plt.show()

if __name__ == "__main__":
    main()
