#!/usr/bin/env python3

import dbconfig

import mysql.connector
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import RegexpTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

def get_posts_list():
    """Get a random list of english posts.

    Returns:
        List of posts
    """
    cnx = mysql.connector.connect(**dbconfig.config)
    cursor = cnx.cursor()
    cursor.execute("SELECT content FROM im_post WHERE id in "
                   "(SELECT id from post_lang_langid WHERE lang='en' AND probability >= 0.9) "
                   "ORDER BY RAND() LIMIT 5000")
    posts_list = []
    for row in cursor:
        posts_list.append(row[0])
    cnx.close()
    return posts_list

def tokenize_and_stem(text):
    """Tokenize and stem the given text.

    Args:
        text: String to tokenize and stem

    Returns:
        List of stems
    """
    #return [stemmer.stem(token) for sentence in nltk.sent_tokenize(text)
    #            for token in nltk.word_tokenize(sentence)
    #                    if len(token) > 1]
    tokenizer = RegexpTokenizer(r"\w+:\/\/\S+|[A-Za-z0-9']+")
    return [stemmer.stem(token) for token in tokenizer.tokenize(text) if len(token) > 3]

def get_cluster_main_words(top_words_num):
    """Get top n words of each cluster

    Args:
        top_words_num: Number of words to be extracted per cluster

    Returns:
        List of list of top words
    """
    order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    terms = tfidf_vectorizer.get_feature_names()
    main_words = []
    for i in range(NUM_CLUSTERS):
        main_words_list = []
        for ind in order_centroids[i, :top_words_num]:
            main_words_list.append(terms[ind])
        main_words.append(main_words_list)
    return main_words

def print_cluster(cluster_num):
    """Print posts of the given cluster

    Args:
        cluster_num: Number of the cluster to print
    """
    i = 0
    for cluster in km.labels_.tolist():
        if cluster == cluster_num:
            print(posts_list[i])
        i += 1


# Number of clusters
NUM_CLUSTERS = 4

CUSTOM_STOP_WORDS = [
    "&gt;",
    "&lt;",
    "...",
    "''",
    '""',
    "``",
    "http",
    "https",
    "'s",
    "look",
    "like",
    "new",
    "n't",
    "get",
    "one",
    "thing",
    "last"
]

stopwords = nltk.corpus.stopwords.words("english")
stopwords.extend(CUSTOM_STOP_WORDS)
stemmer = SnowballStemmer("english")

tfidf_vectorizer = TfidfVectorizer(max_df=0.005, max_features=None,
                                   min_df=0.0001, stop_words=stopwords,
                                   use_idf=True, tokenizer=tokenize_and_stem)
posts_list = get_posts_list()
tfidf_matrix = tfidf_vectorizer.fit_transform(posts_list)
terms = tfidf_vectorizer.get_feature_names()
km = KMeans(n_clusters = NUM_CLUSTERS, max_iter = 100, n_init = 1)
km.fit(tfidf_matrix)

# Print top 10 words of each cluster
i = 0
for cluster in get_cluster_main_words(10):
    print("Cluster " + str(i) + ":")
    i += 1
    for word in cluster:
        print("  " + word)
    print("")
