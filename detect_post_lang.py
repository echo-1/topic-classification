#!/usr/bin/env python3

import dbconfig

import mysql.connector
import multiprocessing
import langid
from concurrent.futures import ThreadPoolExecutor
from langdetect import detect_langs
from langid.langid import LanguageIdentifier, model

# Use 'langid' if true, 'langdetect' otherwise
USE_LANGID = True

POST_TABLE = "im_post"
LANG_TABLE = "TESTLANG"

if USE_LANGID == True:
    identifier = LanguageIdentifier.from_modelstring(model, norm_probs=True)

# Limit memory usage by processing BATCH_SIZE posts at a time
BATCH_SIZE = 1000
THREADS_NUM = multiprocessing.cpu_count()


cnx = mysql.connector.connect(**dbconfig.config)
cursor = cnx.cursor()

def detect_language(full_list, post_id, post_content):
    '''Detect language of the post and add it to full_list'''
    try:
        if USE_LANGID == True:
            (lang, probability) = identifier.classify(post_content)
            full_list.append((post_id, lang, probability))
        else: # langdetect
            result = detect_langs(post_content)
            full_list.append((post_id, result[0].lang, result[0].prob))
    except:
        full_list.append((post_id, None, 0))

# Count number of posts in the database to determine number of loops
cursor.execute("SELECT COUNT(*) FROM " + POST_TABLE)
posts_count = cursor.fetchone()[0]
loops = int(posts_count / BATCH_SIZE) + 1


sql_limit_select = 0
for i in range(loops):
    print("%d/%d" % ((i + 1), loops))
    QUERY_SELECT = ("SELECT id, content FROM " + POST_TABLE + " ORDER BY id LIMIT %s, %s" %
                     (sql_limit_select, BATCH_SIZE))
    cursor.execute(QUERY_SELECT)
    sql_limit_select += BATCH_SIZE

    full_list = []
    with ThreadPoolExecutor(THREADS_NUM) as executor:
        for (post_id, post_content) in cursor:
            executor.submit(detect_language, full_list, post_id, post_content)

    QUERY_INSERT = ("INSERT INTO " + LANG_TABLE + " (id, lang, probability) VALUES "
		    "(%s, %s, %s) ON DUPLICATE KEY UPDATE id=id")
    for dbentry in full_list:
        cursor.execute(QUERY_INSERT, dbentry)
    cnx.commit()

cursor.close()
cnx.close()
